# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.161');


ALTER TABLE `network` ADD COLUMN `NetworkManagerForename` VARCHAR(50) NULL DEFAULT NULL AFTER `AdminSupervisorEmail`;
ALTER TABLE `network` ADD COLUMN `NetworkManagerSurname` VARCHAR(50) NULL DEFAULT NULL AFTER `NetworkManagerForename`;
ALTER TABLE `network` ADD COLUMN `NetworkManagerEmail` VARCHAR(100) NULL DEFAULT NULL AFTER `NetworkManagerSurname`;

# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.162');
