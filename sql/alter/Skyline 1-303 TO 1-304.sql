# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 
call UpgradeSchemaVersion('1.303');

# ---------------------------------------------------------------------- #
# Brian Etherington Changes 											 #
# ---------------------------------------------------------------------- # 
ALTER TABLE `job`
	ADD COLUMN `WarrantyLabourCost` DECIMAL(10,2) NULL DEFAULT NULL,
	ADD COLUMN `WarrantyDeliveryCost` DECIMAL(10,2) NULL DEFAULT NULL;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #
insert into version (VersionNo) values ('1.304');



