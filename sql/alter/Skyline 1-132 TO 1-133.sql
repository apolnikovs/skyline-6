# ---------------------------------------------------------------------- #
# Check Current Database Schema Version No.                              #
# ---------------------------------------------------------------------- # 

call UpgradeSchemaVersion('1.132');


# ---------------------------------------------------------------------- #
# Modify table "manufacturer"                                            #
# ---------------------------------------------------------------------- #

ALTER TABLE `manufacturer`
	CHANGE COLUMN `AuthorisationRequired` `AuthorisationRequired` ENUM('Yes','No') NULL DEFAULT 'No' AFTER `Status`;

UPDATE `manufacturer`
	SET `AuthorisationRequired` = 'No' 
	WHERE `AuthorisationRequired` IS NULL;



# ---------------------------------------------------------------------- #
# Modify table "appointment"                                             #
# ---------------------------------------------------------------------- #

ALTER TABLE `appointment`
	CHANGE COLUMN `CreatedTimeStamp` `CreatedTimeStamp` DATETIME NULL DEFAULT NULL AFTER `ViamenteStartTime`,
	ADD COLUMN `ModifiedDate` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `ForceEngineerToViamente`,
	ADD COLUMN `AppointmentStartTime2` TIME NULL DEFAULT NULL AFTER `ModifiedDate`,
	ADD COLUMN `AppointmentEndTime2` TIME NULL DEFAULT NULL AFTER `AppointmentStartTime2`;


# ---------------------------------------------------------------------- #
# Create table "service_provider_geotags"                                #
# ---------------------------------------------------------------------- #

CREATE TABLE `service_provider_geotags` (
	`ServiceProviderGeoTagsID` INT(11) NOT NULL,
	`ServiceProviderID` INT(11) NOT NULL,
	`Postcode` VARCHAR(8) NULL,
	`Latitude` FLOAT(6,4) NOT NULL,
	`Longitude` FLOAT(7,4) NOT NULL,
	PRIMARY KEY (`ServiceProviderGeoTagsID`),
	INDEX `ServiceProviderID` (`ServiceProviderID`)
)
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;


# ---------------------------------------------------------------------- #
# Update Database Schema Version No.                                     #
# ---------------------------------------------------------------------- #

insert into version (VersionNo) values ('1.133');