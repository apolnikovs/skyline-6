DROP PROCEDURE IF EXISTS `NETWORKDAYS`;
DELIMITER $$
CREATE FUNCTION `NETWORKDAYS`(first_date DATETIME, second_date DATETIME, day_start_time TIME, day_close_time TIME) RETURNS float
    DETERMINISTIC
BEGIN

  -- Calculate Working Days for a job excluding wekends
  -- author     Brian Etherington <b.etherington@pccsuk.com>
  -- copyright  2012 PC Control Systems
  -- link       http://www.pccontrolsystems.com
  -- Version 1.1
  --
  -- Change History
  -- Date       Version   Author                  Reason
  -- 28/12/2012 1.0       Brian Etherington       Initial Version
  -- 30/01/2013 1.1       Brian Etherington       Fix returned units (should be job days not job hours)

  DECLARE start_date DATETIME;
  DECLARE end_date DATETIME;
  DECLARE start_time TIME;
  DECLARE end_time TIME;
  DECLARE daily_start_time TIME;
  DECLARE daily_close_time TIME;
  DECLARE firstday TIME;
  DECLARE lastday TIME;
  DECLARE daydiff INT;
  DECLARE partdays float;
  DECLARE dayminutes INT;
  DECLARE dayhours float;
  DECLARE jobhours float;

  IF (first_date is null or second_date is null) THEN 
    RETURN 0;
  END IF;

  IF (first_date < second_date) THEN
    SET start_date = first_date;
    SET end_date = second_date;
  ELSE
    SET start_date = second_date;
    SET end_date = first_date;
  END IF;

  -- Check if start_date and end_date are the same day

	IF (DATE(start_date) = DATE(end_date)) THEN
    RETURN TIME_TO_SEC(TIMEDIFF(TIME(end_date),TIME(start_date))) / 3600;
  END IF;

  -- Calculate the length of a working day in seconds

  IF (day_start_time is null) THEN
    SET daily_start_time = '09:00:00';
  ELSE
    SET daily_start_time = day_start_time;
  END IF;

  IF (day_close_time is null) THEN
    SET daily_close_time = '17:00:00';
  ELSE
    SET daily_close_time = day_close_time;
  END IF;

  SET dayminutes = TIME_TO_SEC(TIMEDIFF(daily_close_time, daily_start_time)) / 60 ;
  SET dayhours = dayminutes / 60;

  -- Calculate Difference in days

  SET daydiff = DATEDIFF(end_date, start_date);

  -- Calculate part days at beginning and end of event
  
  SET start_time = TIME(start_date);
  IF ( start_time > daily_close_time) THEN
    SET start_time = daily_close_time;
  END IF;
  IF ( start_time < daily_start_time) THEN
    SET start_time = daily_start_time;
  END IF;

  SET end_time = TIME(end_date);
  IF ( end_time < daily_start_time) THEN
    SET end_time = daily_start_time;
  END IF;
  IF ( end_time > daily_close_time) THEN
    SET end_time = daily_close_time;
  END IF;

  SET firstday = TIMEDIFF(daily_close_time, start_time);
  SET lastday = TIMEDIFF(end_time, daily_start_time);
  SET partdays = ((HOUR(firstday) * 60) + MINUTE(firstday) + (HOUR(lastday) * 60) + MINUTE(lastday)) / dayminutes;
  
  SET jobhours = (((daydiff - 1) - (FLOOR(daydiff / 7) * 2)) * 24) + (partdays * dayhours);

  RETURN jobhours / dayhours;

END
$$
DELIMITER ;