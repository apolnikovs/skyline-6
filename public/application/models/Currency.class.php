<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Currency Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0
 */
class Currency extends CustomModel {

    public function __construct($controller) {

        parent::__construct($controller);

        $this->conn = $this->Connect($this->controller->config['DataBase']['Conn'], $this->controller->config['DataBase']['Username'], $this->controller->config['DataBase']['Password']);
    }

    ////currency functions 
    public function getCurrencyList($utID = false) {

        $sql = "select * from currency where Status='Active'";

        $res = $this->query($this->conn, $sql);
        return $res;
    }

    public function insertCurrency($p) {
        $sql = "insert into currency (
            CurrencyName,
            CurrencyCode,
            CurrencySymbol,
            AutomaticExchangeRateCalculation,
            ConversionCalculation,
            ExchangeRate,
            ExchangeRateLastModified,
            ModifiedUserID
            
            )
    values
    (
            :CurrencyName,
            :CurrencyCode,
            :CurrencySymbol,
            :AutomaticExchangeRateCalculation,
            :ConversionCalculation,
            :ExchangeRate,
            now(),
            :ModifiedUserID
           
    
)
    ";
        $params = array(
            'CurrencyName' => $p['CurrencyName'],
            'CurrencyCode' => $p['CurrencyCode'],
            'CurrencySymbol' => $p['CurrencySymbol'],
            'AutomaticExchangeRateCalculation' => (isset($p['AutomaticExchangeRateCalculation']) ? 'Yes' : 'No'),
            'ConversionCalculation' => $p['ConversionCalculation'],
            'ExchangeRate' => $p['ExchangeRate'],
            'ModifiedUserID' => $this->controller->user->UserID
        );
        $this->execute($this->conn, $sql, $params);
    }

    public function updateCurrency($p) {


        $extraSql = "";

        if (!isset($p['AutomaticExchangeRateCalculation'])) {
            $extraSql = ",ExchangeRateLastModified=now()";
            $this->addCurrencyHistoryRecord($p['CurrencyID'], $p['ExchangeRate'], $this->controller->user->UserID, (!isset($p['AutomaticExchangeRateCalculation']) ? 'Yes' : false));
        }
        $sql = "update currency set 
              CurrencyName=:CurrencyName, 
             
              CurrencyCode=:CurrencyCode,
            CurrencySymbol=:CurrencySymbol,
            AutomaticExchangeRateCalculation=:AutomaticExchangeRateCalculation,
            ConversionCalculation=:ConversionCalculation,
            ExchangeRate=:ExchangeRate,
            
            ModifiedUserID=:ModifiedUserID,
             Status=:Status
             $extraSql
                where CurrencyID=:CurrencyID;
    ";

        $params = array(
            'CurrencyName' => $p['CurrencyName'],
            'CurrencyCode' => $p['CurrencyCode'],
            'CurrencySymbol' => $p['CurrencySymbol'],
            'AutomaticExchangeRateCalculation' => (isset($p['AutomaticExchangeRateCalculation']) ? 'Yes' : 'No'),
            'ConversionCalculation' => $p['ConversionCalculation'],
            'ExchangeRate' => $p['ExchangeRate'],
            'ModifiedUserID' => $this->controller->user->UserID,
            'Status' => $p['Status'],
            'CurrencyID' => $p['CurrencyID']
        );
        $this->execute($this->conn, $sql, $params);
    }

    public function deleteCurrency($id) {
        $sql = "update currency set Status='In-Active' where CurrencyID=$id";
        $this->execute($this->conn, $sql);
    }

    //@params 
    //currency id (int)
    //new currency rate (dec)
    //modified user id, or if automatic modify modified site URL (varchar)
    //if setgets username and surname from session else uses $eu value as string
    public function addCurrencyHistoryRecord($cid, $nrate, $eu, $userid = false) {
        $oldData = $this->getCurrencyData($cid);
        //check if rate actualy is changed to avoid dublicates in history
        if ($oldData['ExchangeRate'] != "$nrate") {
            if ($userid) {
                $changeBy = $this->controller->user->Username . " " . $this->controller->user->ContactLastName . " " . $this->controller->user->UserID;
            } else {
                $changeBy = $eu;
            }
            $sql = "insert into currency_history (CurrencyID,ChangeTimestamp,RateFrom,RateTo,ChangedBy)
            values
                (:CurrencyID,now(),:RateFrom,:RateTo,:ChangedBy)
            ";
            $params = array(
                'CurrencyID' => $cid,
                'RateFrom' => $oldData['ExchangeRate'],
                'RateTo' => $nrate,
                'ChangedBy' => $changeBy,
            );
            $this->execute($this->conn, $sql, $params);
        }
    }

    public function getCurrencyData($cid) {
        $sql = "select * from currency where CurrencyID=$cid";
        $res = $this->query($this->conn, $sql);
        if (isset($res[0])) {
            return $res[0];
        } else {
            return false;
        }
    }

    //returns avarage currency value 
    public function getHistoricValue($cid) {
        $sql = "select (sum(RateTo)/count(*)) as avh from currency_history where CurrencyID=$cid";
        $res = $this->query($this->conn, $sql);
        return $res[0]['avh'];
    }

    public function getCurrencyHistory($id) {
        $sql = "select * from currency_history where CurrencyID=$id order by ChangeTimestamp Desc";
        $res = $this->query($this->conn, $sql);
        return $res;
    }

    //converts id to currency code
    public function getCurrencyCode($cId) {
       $sql = "select CurrencyCode from currency where CurrencyID=$cId";
        $res = $this->query($this->conn, $sql);
       
      if(isset($res[0]['CurrencyCode'])){
          return $res[0]['CurrencyCode'];
      }else{
          return "NA";
      }
    }

    public function getSPtoSupplierExchangeRate($spid, $supplierid) {
        //spCurrency
        $sql = "select CurrencyCode from service_provider sps 
            join currency c on c.CurrencyID=sps.CurrencyID
           where sps.ServiceProviderID=$spid";
        $res = $this->query($this->conn, $sql);

        $spCurrency = $res[0]['CurrencyCode'];
        //supplier currency
        $sql = "select CurrencyCode from service_provider_supplier sps 
            join currency c on c.CurrencyID=sps.DefaultCurrencyID
           where sps.ServiceProviderSupplierID=$supplierid 
        ";
        $res = $this->query($this->conn, $sql);

        $supplierCurrency = $res[0]['CurrencyCode'];
        $crModel = $this->controller->loadModel('CurrencyRate');
        $rate = $crModel->getRate($spCurrency, $supplierCurrency);
        return $rate['value'];
    }

    
    public function getSuppliersCurrencyCode($supID){
         $sql = "select CurrencyCode from service_provider_supplier sps 
            join currency c on c.CurrencyID=sps.DefaultCurrencyID
           where sps.ServiceProviderSupplierID=$supID 
        ";
        $res = $this->query($this->conn, $sql);

        $supplierCurrency = $res[0]['CurrencyCode'];
        return $supplierCurrency;
    }    
}

?>
