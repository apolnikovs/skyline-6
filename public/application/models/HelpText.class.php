<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Help Text 
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class HelpText extends CustomModel {
    
    private $conn;
    private $dbColumns = array('HelpTextID', 'HelpTextCode', 'Title', 'Description');
    private $table     = "help_text";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * 
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
        
      
           $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
        
            return  $output;
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['HelpTextID']) || !$args['HelpTextID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
     
    
    
    
    /**
     * Description
     * 
     * This method is used for to validate Help Text Code.
     *
     * @param interger $HelpTextCode 
     * @param interger $HelpTextID.
     * @global $this->table
     * 
     * @return boolean.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
     public function isValidAction($HelpTextCode, $HelpTextID) {
        
         /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT HelpTextID FROM '.$this->table.' WHERE HelpTextCode=:HelpTextCode AND HelpTextID!=:HelpTextID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        $fetchQuery->execute(array(':HelpTextCode' => $HelpTextCode, ':HelpTextID' => $HelpTextID));
        $result = $fetchQuery->fetch();
        
        if(is_array($result) && $result['HelpTextID'])
        {
                return false;
        }
        
        return true;
    
    }
    
    
    /**
     * Description
     * 
     * This method is used for to insert data into database.
     *
     * @param array $args  
     * @global $this->table 
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function create($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'INSERT INTO '.$this->table.' (HelpTextCode, HelpTextTitle, HelpText, Status, CreatedDate, ModifiedUserID, ModifiedDate)
            VALUES(:HelpTextCode, :HelpTextTitle, :HelpText, :Status, :CreatedDate, :ModifiedUserID, :ModifiedDate)';
        
        
        if($this->isValidAction($args['HelpTextCode'], 0))
        {
            $insertQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
          
            
            $insertQuery->execute(array(':HelpTextCode' => $args['HelpTextCode'], ':HelpTextTitle' => $args['HelpTextTitle'], ':HelpText' => $args['HelpText'], ':Status' => $args['Status'], 
                
                ':CreatedDate' => date("Y-m-d H:i:s"),
                ':ModifiedUserID' => $this->controller->user->UserID,
                ':ModifiedDate' => date("Y-m-d H:i:s")
                
                ));
        
        
              return array('status' => 'OK',
                        'message' => "Data has been inserted successfully.");
        }
        else
        {
            
            return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        if(isset($args['HelpTextCode']))
        {
            $sql = 'SELECT HelpTextID, HelpTextCode, HelpTextTitle, HelpText, Status, EndDate FROM '.$this->table.' WHERE HelpTextCode=:HelpTextCode';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':HelpTextCode' => $args['HelpTextCode']));
            
        }
        else
        {
            $sql = 'SELECT HelpTextID, HelpTextCode, HelpTextTitle, HelpText, Status, EndDate FROM '.$this->table.' WHERE HelpTextID=:HelpTextID';
            $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
            $fetchQuery->execute(array(':HelpTextID' => $args['HelpTextID']));
        }
        
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
      /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($this->isValidAction($args['HelpTextCode'], $args['HelpTextID']))
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            if($args['Status']=="In-active")
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
                else
                {
                        $EndDate = $row_data['EndDate'];
                }
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              HelpTextCode=:HelpTextCode, HelpTextTitle=:HelpTextTitle, HelpText=:HelpText,  Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate

              WHERE HelpTextID=:HelpTextID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        
                        ':HelpTextCode' => $args['HelpTextCode'], 
                        ':HelpTextTitle' => $args['HelpTextTitle'],   
                        ':HelpText' => $args['HelpText'],  
                        ':Status' => $args['Status'],
                        ':EndDate' => $EndDate,
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':HelpTextID' => $args['HelpTextID']
                
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => "Data has been updated successfully.");
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
    
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => $this->controller->page['Text']['data_deleted_msg']);
    }
    
    
    
}
?>