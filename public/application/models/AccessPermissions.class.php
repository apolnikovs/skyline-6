<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Access Permissions Page in Lookup Tables section under System Admin
 *
 * @author      Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
 * @version     1.0
 */

class AccessPermissions extends CustomModel {
    
    private $conn;
    private $dbColumns = array('PermissionID', 'Name', 'Description', 'Status');
    private $table     = "permission";
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] );       

    }
    
   
     /**
     * Description
     * 
     * This method is for fetching data from database
     * 
     * @param array $args Its an associative array contains where clause, limit and order etc.
     * @global $this->conn
     * @global $this->tables
     * @global $this->dbColumns
     * @return array 
     * 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */  
    public function fetch($args) {
        
        
      
           $output = $this->ServeDataTables($this->conn, $this->table, $this->dbColumns, $args);
        
        
            return  $output;
        
     }
    
    
     /**
     * Description
     * 
     * This method calls update method if the $args contains primary key.
     * 
     * @param array $args Its an associative array contains all elements of submitted form.
    
     * @return array It contains status and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com> 
     */   
    
     public function processData($args) {
         
         if(!isset($args['PermissionID']) || !$args['PermissionID'])
         {
               return $this->create($args);
         }
         else
         {
             return $this->update($args);
         }
     }
    
     
    
     /**
     * Description
     * 
     * This method is used for to fetch a row from database.
     *
     * @param array $args
     * @global $this->table  
     * @return array It contains row of the given primary key.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function fetchRow($args) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT PermissionID, Name, Description, Status, EndDate FROM '.$this->table.' WHERE PermissionID=:PermissionID';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':PermissionID' => $args['PermissionID']));
        $result = $fetchQuery->fetch();
        
        return $result;
    }
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to fetch tagged permissions count for given user role id.
     *
     * @param int $RoleID
     * 
     
     * @return int 
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function userRolePermissions($RoleID) {
        
        
        /* Execute a prepared statement by passing an array of values */
        $sql = 'SELECT COUNT(*) FROM assigned_permission AS T1 LEFT JOIN permission AS T2 ON T1.`PermissionID` = T2.`PermissionID` WHERE T1.RoleID=:RoleID AND T1.Status=:Status AND T2.Status=:Status1';
        $fetchQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
        
        
        $fetchQuery->execute(array(':RoleID' => $RoleID, ':Status' => 'Active', ':Status1' => 'Active'));
        $result = $fetchQuery->fetch();
        
        
        
        if(isset($result[0]))
        {
            return $result[0];
        }
        else
        {
            return 0;
        }
        
    }
    
    
    
    
    
    
     /**
     * Description
     * 
     * This method is used for to udpate a row into database.
     *
     * @param array $args
     * @global $this->table   
     * @return array It contains status of operation and message.
     * @author Nageswara Rao Kanteti <nag.phpdeveloper@gmail.com>
     */ 
    public function update($args) {
        
        if($args['PermissionID'])
        {        
            
            $EndDate = "0000-00-00 00:00:00";
            $row_data = $this->fetchRow($args);
            
            if($args['Status']=='In-active')
            {
                if($row_data['Status']!=$args['Status'])
                {
                        $EndDate = date("Y-m-d H:i:s");
                }
            }
            else
            {
                $EndDate = $row_data['EndDate'];
            }
            
               /* Execute a prepared statement by passing an array of values */
              $sql = 'UPDATE '.$this->table.' SET 
                
              Name=:Name, Description=:Description, Status=:Status, EndDate=:EndDate, ModifiedUserID=:ModifiedUserID, ModifiedDate=:ModifiedDate

              WHERE PermissionID=:PermissionID';
        
              $updateQuery = $this->conn->prepare($sql, array(PDO::ATTR_CURSOR => PDO::CURSOR_FWDONLY));
              $updateQuery->execute(
                      
                      array(
                        
                        ':Name' => $args['Name'], 
                        ':Description' => $args['Description'],   
                        ':Status' => $args['Status'],
                        ':EndDate' => $EndDate,
                        ':ModifiedUserID' => $this->controller->user->UserID,
                        ':ModifiedDate' => date("Y-m-d H:i:s"),
                        ':PermissionID' => $args['PermissionID']
                
                )
                      
             );
        
                
               return array('status' => 'OK',
                        'message' => 'Your data has been updated successfully.');
        }
        else
        {
             return array('status' => 'ERROR',
                        'message' => $this->controller->messages->getError(1024, 'default', $this->controller->lang));
        }
    }
    
    
   
    
    
    public function delete(/*$args*/) {
        return array('status' => 'OK',
                     'message' => 'Your data has been deleted successfully.');
    }
    
    
    
    
    
    
}
?>