<?php

require_once('CustomModel.class.php');
require_once('TableFactory.class.php');

/**
 * Description
 *
 * This class is used for handling database actions of Stock Page in Lookup Tables section under System Admin
 *
 * @author      Andris Polnikovs <a.polnikovs@gmail.com>
 * @version     1.0
 */

class Stock extends CustomModel {
    
   
    
      
    public function __construct($controller) {
    
        parent::__construct($controller); 

        $this->conn = $this->Connect( $this->controller->config['DataBase']['Conn'],
                                      $this->controller->config['DataBase']['Username'],
                                      $this->controller->config['DataBase']['Password'] ); 
        $this->SQLGen=$this->controller->loadModel('SQLGenerator');
        $this->fields=[
            "ServiceProviderID",
            "PartNumber",
            "PurchaseCost",
            "Description",
            "MinStock",
            "MakeUpTo",
            "ServiceProviderColourID",
            "PrimaryServiceProviderModelID",
            "DefaultServiceProviderSupplierID",
            "Status",
           
                ];

    }
    
   
    
   ////stock functions 
    public function getStockList($utID=false){
       
            $sql="select * from stock where Status='Active'";
     
        $res=$this->query( $this->conn, $sql); 
        return $res;
    }
    
    
    public function insertPartStockTemplate($P,$spid){
       if(!isset($P["DefaultServiceProviderSupplierID"])){
           $P["DefaultServiceProviderSupplierID"]=$P['ServiceProviderSupplierID'];
       }
        $val=
        [
           $spid,
           $P["PartNumber"],
           $P["PurchaseCost"],
           $P["Description"],
           $P["MinStock"],
           $P["MakeUpTo"],
           $P["ServiceProviderColourID"],
           $P["PrimaryServiceProviderModelID"],
           $P["DefaultServiceProviderSupplierID"],
            isset($P["Status"])?"In-active":"Active"
        ];
       $id=$this->SQLGen->dbInsert('sp_part_stock_template',$this->fields,$val); 
       return $id;
    }
    
    public function updatePartStockTemplate($P,$spid){
        if(!isset($P["DefaultServiceProviderSupplierID"])){
           $P["DefaultServiceProviderSupplierID"]=$P['ServiceProviderSupplierID'];
       }
          $val=
        [
           $spid,
           $P["PartNumber"],
           $P["PurchaseCost"],
           $P["Description"],
           $P["MinStock"],
           $P["MakeUpTo"],
           $P["ServiceProviderColourID"],
           $P["PrimaryServiceProviderModelID"],
           $P["DefaultServiceProviderSupplierID"],
           isset($P["Status"])?"In-active":"Active"
        ];
       $this->SQLGen->dbUpdate('sp_part_stock_template',$this->fields,$val,"SpPartStockTemplateID=".$P['SpPartStockTemplateID']); 
    }
    
    
    //fetch all infromation for part stock template
    public function getData($id,$table){
   $sql="select *,date_format(spo.ReceviedDate,'%d/%m/%Y') as RecDate,
           
                (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                
                where 
                sppsi.SPPartStockTemplateID=$id
                 and sps.InStock='Y'    
                )as InStock,
                (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$id
                and sps.Available='Y' and sps.InStock='Y'    
                )as `InStockAvailable`
                ,t.PurchaseCost
            from $table t
            left join sp_part_stock_item spsi on spsi.SPPartStockTemplateID=t.SPPartStockTemplateID
            left    join sp_part_order spo on spo.SPPartOrderID=spsi.SPPartOrderID
        where t.SpPartStockTemplateID=$id 
            and spsi.SPPartStatusID in (1,9)
           
            order by spsi.SPPartStockItem asc 
            limit 1";
       $res=$this->Query($this->conn, $sql);
    if(sizeof($res)==0){
       $sql="select *,date_format(spo.ReceviedDate,'%d/%m/%Y') as RecDate,  (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                
                where 
                sppsi.SPPartStockTemplateID=$id
                 and sps.InStock='Y'    
                )as InStock,
                (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$id
                and sps.Available='Y' and sps.InStock='Y'    
                )as `InStockAvailable`
                 ,t.PurchaseCost
            from $table t
            left join sp_part_stock_item spsi on spsi.SPPartStockTemplateID=t.SPPartStockTemplateID
            left    join sp_part_order spo on spo.SPPartOrderID=spsi.SPPartOrderID
        where t.SpPartStockTemplateID=$id 
           
           
            order by spsi.SPPartStockItem asc 
            limit 1";
       $res=$this->Query($this->conn, $sql);
    }
       
     if(isset($res[0])){
         return $res[0];
     }else{
         false;
     }
   }
  
    public function deleteStock($id){
        $this->SQLGen->dbMakeInactive('sp_part_stock_template',"SpPartStockTemplateID=$id"); 
    }
    
     
    public function checkOrderNo($OrderNo,$spid){
       $sql="select * from sp_part_order spo 
           left join sp_part_stock_item spsi on spsi.SPPartOrderID=spo.SPPartOrderID
           left join part p on p.PartID=spsi.PartID
          left  join sp_part_stock_template spst on spst.SpPartStockTemplateID=spsi.SPPartStockTemplateID
          where spo.OrderNo='$OrderNo' and spo.ServiceProviderID=$spid
            ";
       $res=$this->Query($this->conn, $sql);
    
     if(isset($res[0])){
         return $res[0];
     }else{
         false;
     }
    }
    
   public function getSpPartTemplates($spid){
       $sql="select * from sp_part_stock_template where ServiceProviderID=$spid and Status='Active'";
        $res=$this->Query($this->conn, $sql);
        return $res;
   }
    
    
   public function getSpPartTemplateData($spid,$number) {
       $sql="select * from sp_part_stock_template
            
                where ServiceProviderID=$spid and PartNumber='$number'";
        $res=$this->Query($this->conn, $sql);
        if(isset($res[0])){
         return $res[0];
     }else{
         false;
     }
   }
   public function getSpPartTemplateDataFromId($spid,$id) {
       $sql="select * from sp_part_stock_template
            
                where ServiceProviderID=$spid and SpPartStockTemplateID='$id'";
        $res=$this->Query($this->conn, $sql);
        if(isset($res[0])){
         return $res[0];
     }else{
         false;
     }
   }
   //this is used when receiving order witch is not linked to any jobs
   public function insertStockItem($spid,$p,$statusID=false,$orderID=false){
       if(!$orderID){
       //check if order exists
       if(!$orderID=$this->checkOrderNo($p['OrderNo'],$spid)){
           //if no create one
             
              $fields=[
                  "ServiceProviderSupplierID",
                  "OrderNo",
                  "ReceviedDate",
                  "Received",
                  "ReceivedByUserID",
                  "ServiceProviderID",
              ];
              
         $val=[
           $p["ServiceProviderSupplierID"],
           $p["OrderNo"],
           date('Y-m-d H:i:s'),
           "Y",
           $this->controller->user->UserID,
            $spid
           ];
       $orderID=$this->SQLGen->dbInsert('sp_part_order',$fields,$val); 
           
       }else{
           $orderID=$orderID['SPPartOrderID'];
       }
       
       }
       
       
        //check if stock template exist exist
       if(!$template_ID=$this->getSpPartTemplateData($spid,$p['PartNumber'])){
           //if no create one
           $template_ID=$this->insertPartStockTemplate($p, $spid);
       }else{
           $template_ID=$template_ID['SpPartStockTemplateID'];
       }
       
       if(!$statusID){
           $statusID=1;
       }
       
       //inserting items and creating history entry
       for($i=0;$i<$p['qty'];$i++){
           $f=[
             "SPPartStockTemplateID",  
             "SPPartStatusID",  
             "PurchaseCost",
             "SPPartOrderID"
           ];
           $v=[
               $template_ID,
               $statusID,
               $p['PurchaseCost'],
               $orderID
           ];
           $itemID=$this->SQLGen->dbInsert('sp_part_stock_item',$f,$v); 
            
            
         //add history record
        $this->insertStockItemHistory($itemID,$statusID,$p['OrderNo']);
          
       }
       
       
   }
   
  
   //decrement parts from stock
   public function makePartsUsed($p)
   {
        $limit=$p['Quantity'];
       if($limit==''){
           $limit=0;
       }
       if($p['PartID']==""){
       //insert part
      
       $sql="select * from sp_part_stock_item s 
           where 
           s.SPPartStockTemplateID=:SPPartStockTemplateID 
          
           and SPPartStatusID in (1,9) limit $limit";
       
       $param=[
         "SPPartStockTemplateID"=>$p['SpPartStockTemplateID']  
       ];
      // $this->controller->log($_POST);
      $res=$this->Query($this->conn, $sql,$param);
     
      foreach ($res as $r){
       
        //set item status
        $this->updateStockPartItemStatus($r['SPPartStockItem'],11,$p['JobID']);
         //add history record
        $this->insertStockItemHistory($r['SPPartStockItem'],11,$p['JobID']);
           
      }
      if(sizeof($res)<$p['Quantity']){
          //order missing parts
          $orderNeeded=$p['Quantity']-sizeof($res);//getting mising parts count
          
          $pa=[
              'qty'=>$orderNeeded,
              'SpPartStockTemplateID'=>$p['SpPartStockTemplateID'],
              'JobID'=>$p['JobID']
          ];
          //$this->insertStockOrderReqItem($this->controller->user->ServiceProviderID,$pa);
          return true;//part items cretaed with status 2 parts order required;
      
      }
      return true;//no parts ordering required
   }else{
       //update part
       $jobid=$p['JobID'];
       
       $sql="select count(*) as c from sp_part_stock_item spsh where spsh.JobID=$jobid and spsh.SPPartStatusID=11 and SPPartStockTemplateID=:SPPartStockTemplateID ";
       $param=[
         "SPPartStockTemplateID"=>$p['SpPartStockTemplateID']  
       ];
       $res=$this->Query($this->conn, $sql,$param);
       $dif=$res[0]['c']-$limit;//getting difrence between parts already used ont this job and parts used after update
       if($dif<0){
           $limit=$dif*-1;
           //more parts being used
           $sql="select * from sp_part_stock_item s 
           where 
           s.SPPartStockTemplateID=:SPPartStockTemplateID 
           and JobID is null
           and SPPartStatusID in (1,9) limit $limit";
       
       $param=[
         "SPPartStockTemplateID"=>$p['SpPartStockTemplateID']  
       ];
       //$this->controller->log($_POST);
      $res=$this->Query($this->conn, $sql,$param);
      if(sizeof($res)==$limit){//check if selected enought parts
     foreach ($res as $r){
        //set item status
        $this->updateStockPartItemStatus($r['SPPartStockItem'],11,$p['JobID']);
         //add history record
        $this->insertStockItemHistory($r['SPPartStockItem'],11,$p['JobID']);
      }
           return true;
       }else{
           return false;
       }
       }
       
       
       if($dif>0){
           //less parts used
            $limit=$dif;
           //more parts being used
           $sql="select * from sp_part_stock_item s 
           where 
           s.SPPartStockTemplateID=:SPPartStockTemplateID 
           and JobID=:JobID
           and SPPartStatusID=11 limit $limit";
       
       $param=[
         "SPPartStockTemplateID"=>$p['SpPartStockTemplateID'],
         "JobID"=>$p['JobID'],
           
       ];
     
      $res=$this->Query($this->conn, $sql,$param);
      
     foreach ($res as $r){
       
              //set item status
        $this->updateStockPartItemStatus($r['SPPartStockItem'],1,$p['JobID']);
         //add history record
        $this->insertStockItemHistory($r['SPPartStockItem'],1,$p['JobID']);
      }
       }
       return true;
   }
   
   }
   
   //if part deleted from job need make it available in stock
   public function puttPartBackToStock($partID,$JobID){
       $sql="select SpPartStockTemplateID,Quantity,PartStatusID from part where PartID=$partID";
      
       $resPart=$this->Query($this->conn, $sql);
       $limit=$resPart[0]['Quantity'];
       $sql="select * from sp_part_stock_item where SPPartStockTemplateID=:SPPartStockTemplateID and SPPartStatusID=:PartStatusID and JobID=:JobID limit $limit";
       $param=[
           "SPPartStockTemplateID"=>$resPart[0]['SpPartStockTemplateID'],
           "JobID"=>$JobID,
           "PartStatusID"=>$resPart[0]['PartStatusID'],
       ];
       
       $res=$this->Query($this->conn, $sql,$param);
       switch ($resPart[0]['PartStatusID']){
           
           case 11:{
       
       foreach ($res as $r){
       
          //set item status
        $this->updateStockPartItemStatus($r['SPPartStockItem'],1);
         //add history record
        $this->insertStockItemHistory($r['SPPartStockItem'],1,$r['JobID']);
      }
      break;
           }
      case 3:     
      case 2:{
          //if part with status 02 order required is deleted from job, delete any items with same status 
          foreach ($res as $r){
            $sql="delete from sp_part_stock_history where SPPartStockItemID=:SPPartStockItem";
         
             $params=[
              "SPPartStockItem"=>$r['SPPartStockItem']
              
              ];
          $this->Execute($this->conn, $sql,$params);
          $sql="delete from sp_part_order where SPPartOrderID=(select SPPartOrderID from sp_part_stock_item where SPPartStockItem=:SPPartStockItem)";
           $this->Execute($this->conn, $sql,$params);
             $sql="delete from sp_part_stock_item where SPPartStockItem=:SPPartStockItem";
             $this->Execute($this->conn, $sql,$params);
             
          }
       break;
            }
       }
   }
    
   public function getOrderHistoryData($id,$sp){
       $sql="select *,concat_ws(' ',u.ContactFirstName,u.ContactLastName) as ReceivedBy,
            date_format(spo.CreatedDate,'%d/%m/%Y (%H:%i)') as ReceivedDate
            from sp_part_order  spo 
            join service_provider_supplier sps on sps.ServiceProviderID=spo.ServiceProviderID
            join user u on u.UserID=spo.ReceivedByUserID
            where SPPartOrderID=$id and spo.ServiceProviderID=$sp";
         $res=$this->Query($this->conn, $sql);
         return $res;
       
   }
   public function getStockItemQty($id){
       if($id==''){
           return 999999;//if part is inserted manualy(part template id is not set) ignore qty check
       }
       $sql="select count(*) as available from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=$id
                and sps.Available='Y' and sps.InStock='Y'";
         $res=$this->Query($this->conn, $sql);
         if(isset($res[0])){
         return $res[0]['available'];
         //return 1;
         }else{return 0;}
       
   }
   
   
   public function getExportData($type,$sp,$dFrom=false,$dTo=false){
       
       $dbtmp=explode(';',$this->controller->config['DataBase']['Conn']);
 $db= substr($dbtmp[1],7);
 $dbhost=substr($dbtmp[0],11);
 $dbc = mysql_connect( $dbhost , $this->controller->config['DataBase']['Username'] , $this->controller->config['DataBase']['Password']) or die( mysql_error() );
       mysql_select_db( $db);
                
                
                if($type==2){
                    $where=" and (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=st.SpPartStockTemplateID
                 and sps.InStock='Y'    
                ) <= st.MinStock and st.MinStock!='0'";}else{$where="";
                
                            }
                            
     if($type==2||$type==1)
         {
   $q="select 
            st.PartNumber as `Stock No`,
            st.Description as `Description`,
            (select count(*) from sp_part_stock_item sppsi 
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID
                where 
                sppsi.SPPartStockTemplateID=st.SpPartStockTemplateID
                 and sps.InStock='Y'    
                )as `In Stock`,
            st.MinStock as `Minimum Level`,
            st.MakeUpTo as `Make up To`,
            (select PurchaseCost from sp_part_stock_item spsi where spsi.SPPartStockTemplateID=st.SpPartStockTemplateID order by spsi.CreatedDate desc limit 1) as `Last Purchase Cost`,
            m.ModelNumber as `Model`,
            c.ColourName as `Colour`,
            
            (if((st.MakeUpTo-(select count(*) from sp_part_stock_item sppsi 		
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID		
                where 		
                sppsi.SPPartStockTemplateID=st.SpPartStockTemplateID		
                 and sps.InStock='Y'    		
                ))<0	,0,	st.MakeUpTo-(select count(*) from sp_part_stock_item sppsi 		
                join part_status sps on sps.PartStatusID=sppsi.SPPartStatusID		
                where 		
                sppsi.SPPartStockTemplateID=st.SpPartStockTemplateID		
                 and sps.InStock='Y'    		
                ))
                ) as `Reorder Quantity`	
     from sp_part_stock_template st
    left join service_provider_model m on m.ServiceProviderModelID=st.PrimaryServiceProviderModelID
    left join service_provider_colour c on c.ServiceProviderColourID=st.ServiceProviderColourID
     WHERE st.Status='Active' AND st.ServiceProviderID=$sp $where";    
         }
         
      if($type==3){
          
          $timestamp = strtotime(str_replace('/', '.', $dFrom));
          $dFrom = date('Y-m-d', $timestamp); 
          $timestamp = strtotime(str_replace('/', '.', $dTo));
          $dTo = date('Y-m-d', $timestamp); 

         
         
        $q="select 
              PartNumber as `Stock Code`,
              Description as `Description`
              ,(select count(*) from sp_part_stock_item s 
join sp_part_stock_history sh on sh.SPPartStockItemID=s.SPPartStockItem
where  s.SPPartStockTemplateID=st.SpPartStockTemplateID
and s.SPPartStatusID in(11) 
and s.JobID is not null 
and sh.DateTime>='$dFrom'
and sh.DateTime<='$dTo' ) as `Qty Used`
              from sp_part_stock_template st";
      }   
                
      
                
  $qr = mysql_query( $q ) or die(mysql_error());
 //$qr=$this->Query($this->conn, $q); //this used for xls export
   return $qr;               
           
   }
   
   
   public function checkStockAvailable($spid){
       $sql="select * from sp_part_stock_template where ServiceProviderID =$spid";
       $res=$this->Query($this->conn, $sql);
         if(isset($res[0])){
         return 'true';
         //return 1;
         }else{return false;}
   }
   
   
   public function insertStockOrderReqItem($spid,$p,$partid=false){
        //check if stock template exist exist
       $template_ID=$p['SpPartStockTemplateID'];
           //if no create one
      
       //inserting items and creating history entry
       for($i=0;$i<$p['qty'];$i++){
           $f=[
             "SPPartStockTemplateID",  
             "SPPartStatusID",  
             "PurchaseCost",
             "SPPartOrderID",
             "JobID"
           ];
           if($partid){$f[]="PartID";}
           if(!isset($p['SPPartOrderID'])){$p['SPPartOrderID']=null;}
           $v=[
               $template_ID,
               2,
               null,
               $p['SPPartOrderID'],
               $p['JobID']
           ];
           if($partid){$v[]=$partid;}
           $itemID=$this->SQLGen->dbInsert('sp_part_stock_item',$f,$v);
           //history
          
           $this->insertStockItemHistory($itemID,2,$p['JobID']);
          
       }
       
       
   }
   
   //gets list of statuses for ordering table returns array
   public function getOrderingStatuses(){
           $sql="select * from part_status where PartStatusID in (2,6)";
           return $this->Query($this->conn, $sql);
       }
       
       
       //used to change part status
       //arg1=array of parts ids
       //arg2=part status id
      
  public function changePartsStatus($p,$s){
      $part_model=$this->controller->loadModel('Part');
      $this->controller->log($p,"ada");
     
      foreach($p as $f){
          $part_data=$part_model->getPartByID($f);
          if($f!="0"){
          $items=$this->getAllPartItemsByPartID($f);
          }else{
              break;
          }
             
            //updating stock item status
            foreach($items as $t){
                
                
             
                  //set item status
        $this->updateStockPartItemStatus($t['SPPartStockItem'],$s);
         //add history record
        $this->insertStockItemHistory($t['SPPartStockItem'],$s,$part_data['JobID']);
            }
           
          //updating parts
          switch ($s){
              case 6:{
                  $sql="update part set PartStatusID=:PartStatusID,Quantity=0 where PartID=:PartID";
                  //if part no more available change open job status to 98 
                   $job_model=$this->controller->loadModel('Job');
                   $job_model->setJobStatusByID($part_data['JobID'], 48);
              break;}
              default: 
                  $sql="update part set PartStatusID=:PartStatusID where PartID=:PartID";
              break;
          }
          
          $params=[
              "PartStatusID"=>$s,
              "PartID"=>$f,
              ];
         $this->Execute($this->conn, $sql,$params); 
         //updating stockItemsStatuses
         
      }
      
    
  }
  //used to insert history record for items status change
  //arg1=part stock item id
  //arg2=part status id
  //arg3=reference number
  
  public function insertStockItemHistory($i,$s,$r=null)
   {
        $f=[
              "SPPartStockItemID", "SPPartStatusID" ,"UserID","Reference","DateTime"
            ];
           $v=[
               $i,$s,$this->controller->user->UserID,$r,date('Y-m-d H:i:s')
           ];
             
           $this->SQLGen->dbInsert('sp_part_stock_history',$f,$v,false);
  }
  //used to insert history record for items status change
  //arg1=part stock item id
  //arg2=part status id
  //arg3=jobid
  //arg4=orderid
  public function updateStockPartItemStatus($i,$s,$j=false,$o=false){
       $params=[
              "SPPartStockItem"=>$i,
             
              "SPPartStatusID"=>$s
             
              ];
      $job="";
      $order="";
      if($j){
          $job=",JobID=:JobID";
          $params["JobID"]=$j;
      }
      if($o){
          $order=",SPPartOrderID=:SPPartOrderID";
          $params["SPPartOrderID"]=$o;
      }
      $sql="update sp_part_stock_item set SPPartStatusID=:SPPartStatusID $job $order where SPPartStockItem=:SPPartStockItem";
          
          $this->Execute($this->conn, $sql,$params);
  }
  
  //create orders for selected parts
 public function MakeTaggedOrders($parts){
     //creating array of parts for each order separated by suppplier
     foreach($parts as $p){
         $data=$this->getAllPartData($p);
        
         $suplier=$data['DefaultServiceProviderSupplierID'];//needs change when multiple suppliers available
         $templateID=$data['SpPartStockTemplateID'];
         if($data['Quantity']>0)
         {
             if(isset($orders[$suplier][$templateID])){
                 $orders[$suplier][$templateID]=$orders[$suplier][$templateID]+$data['Quantity'];
             }else{
                 $orders[$suplier][$templateID]=$data['Quantity'];
             }
             $this->controller->log($data);
           $items=$this->getAllPartItemsByPartID($p);  
             ///this TODO need finishing
           
         }
     }
     
     //creating orders records
     foreach($orders as $o=>$i){
        $this->createSingleGSPNPartOrder($o);
     }
     
     
     
      
 }
 
 public function getAllPartData($pid){
     $sql="select * from part p 
    join sp_part_stock_template spst on spst.SpPartStockTemplateID=p.SpPartStockTemplateID     
    where p.PartID=$pid";
      $res=$this->Query($this->conn, $sql);
      return $res[0];
 }
 //get all stock items related to this part record
 //arg1=jobid
 //arg2=templateid
 //arg3=statusid
 public function getAllPartItems($j,$t,$s){
      //geting stock items related to this part
          $sql="select * from sp_part_stock_item spsi where 
              spsi.JobID=:JobID 
              and spsi.SPPartStockTemplateID=:SPPartStockTemplateID 
              and spsi.SPPartStatusID=:SPPartStatusID";
          $params=[
             "JobID"=>$j, 
             "SPPartStockTemplateID"=>$t, 
             "SPPartStatusID"=>$s, 
          ];
           $items=$this->Query($this->conn, $sql,$params); 
           return $items;
 }
 
 public function getAllPartItemsByPartID($pid){
     $sql="select * from sp_part_stock_item where PartID=$pid";
     return $this->Query($this->conn, $sql); 
 }
 
public function setItemPartID($partID,$partStatus){
    $p=$this->getAllPartData($partID);
    $items=$this->getAllPartItems($p['JobID'],$p['SpPartStockTemplateID'],$partStatus);
    foreach($items as $i){
        $itemId=$i['SPPartStockItem'];
       $sql="update sp_part_stock_item set PartID=$partID where SPPartStockItem=$itemId";
       $this->Execute($this->conn, $sql); 
    }
}


public function createSingleGSPNPartOrder($SPSupplierID){
 
         $f=[
              "ServiceProviderSupplierID" ,"OrderedDate","ServiceProviderID","SPPartOrderStatusID"
            ];
         $v=[
               $SPSupplierID,date('Y-m-d H:i:s'),$this->controller->user->ServiceProviderID,1
           ];
          $orderID=$this->SQLGen->dbInsert('sp_part_order',$f,$v);
          //adding order number to order
          $f=["OrderNo"];
          $v=[$orderID];
          $this->SQLGen->dbUpdate('sp_part_order',$f,$v,"SPPartOrderID=$orderID");
          return $orderID;
     }
     
 public function setSamsungOrderNo($samsungNo,$oldOrderNo,$partid,$ExpectedDate){
     $ExpectedDate=date( 'Y-m-d', strtotime ($ExpectedDate));
     //updating order record
     $sql="update sp_part_order set OrderNo='$samsungNo' where OrderNo='$oldOrderNo'";
     $this->Execute($this->conn, $sql);
     //updating part record
      $sql="update part set OrderNo=$samsungNo,OrderDate=now(),DueDate='$ExpectedDate' where PartID=$partid";
     $this->Execute($this->conn, $sql);
 }
  
 
 public function receiveGSPNStockItem($p,$o){
       //SPPartOrderID
     //TODO need include quantity check
     if($o['JobID']==""){
         $status=1;//item in stock
     }else{
         $status=4;//item delivered for specific job
     }
     $this->changePartsStatus(array($o['PartID']),$status);
       $sql="update sp_part_order set ReceviedDate=now(),Received='Y',ReceivedByUserID=:ReceivedByUserID,SPPartOrderStatusID=2 where SPPartOrderID=:SPPartOrderID";
       $param=[
           'ReceivedByUserID'=>$this->controller->user->UserID,
           'SPPartOrderID'=>$o['SPPartOrderID']
           ];
        $this->Execute($this->conn, $sql,$param);
        $sql="update part set ReceivedDate=now() where PartID=:PartID";
        $param=[
         
           'PartID'=>$o['PartID']
           ];
        $this->Execute($this->conn, $sql,$param);
        //check if job got other parts with status ordered or order required
       
        if($o['JobID']!=""){
        $sql="select * from part where JobID=:JobID and PartStatusID in (2,3)";
        $param=["JobID"=>$o['JobID']];
        $res=$this->Query($this->conn, $sql,$param);
        //if there is no other parts with status 2 or 3 change job status to parts received
        if(!isset($res[0])){
            $job_model=$this->controller->loadModel('Job');
            $job_model->setJobStatusByID($o['JobID'], 20);
        }
        }
   }
    public function getPartDataFromOrderNo($orderNo){
        $sql="select * from part where OrderNo='$orderNo'";
            $res=$this->Query($this->conn, $sql); 
         if(isset($res[0])){
         return $res[0];
         //return 1;
         }else{return false;}
    }
    
     //this is used to link model to stock template
    public function addModel($m, $a) {
        $sql = "insert into service_provider_model_to_sp_part_stock_template  (SpPartStockTemplateID,ServiceProviderModelID) values ($m,$a)";
        $this->execute($this->conn, $sql);
    }

    //this is used to unlink model to stock template
    public function delModel($m, $a) {
        $sql = "delete from service_provider_model_to_sp_part_stock_template  where SpPartStockTemplateID=$m and ServiceProviderModelID=$a";
        $this->execute($this->conn, $sql);
    }

    public function loadModel($m) {
        $sql = "select m.ModelNumber as `name`
,m.ServiceProviderModelID as `id`
 from service_provider_model m 
    join service_provider_model_to_sp_part_stock_template am on am.ServiceProviderModelID=m.ServiceProviderModelID
                    
                where am.SpPartStockTemplateID=$m";
        $this->controller->log($sql);
        $res = $this->query($this->conn, $sql);
        return $res;
    }
     


}
?>