<script type="text/javascript" >
  
$(document).ready(function() {

     $('#p_wildCard').click(function(event) {
     
        if(!$('#productSearch').val())
        {
            $('#productSearch').focus();
            $("[for='productSearch']").css("display", "none");
        }
        
        insertAtCursor(document.productSearchForm.productSearch, "*");
        
        
     });
     $("#p_wildCardForm").hide();
     $("#p_wildCardHelp").colorbox(

                    { 
                            inline:true, 
                            href:"#p_wildCardForm", 
                            title: 'Wild Card Search',
                            opacity: 0.75,
                            height:290,
                            width:700,
                            overlayClose: false,
                            escKey: false,
                            onOpen:function(){
                                $("#p_wildCardForm").show("fast");
                            },                            
                            onCleanup:function(){
                                $("#p_wildCardForm").hide("fast");
                            }

                    }
                );
                
     
     
     //click handler for free text job booking link starts here.
        $(document).on('click', "#FreeTextJobBooking", 
            function() {
                          
                          
                        //It opens color box popup page.              
                        $.colorbox( { href: '{$_subdomain}/Job/networkClientPopUp//'+ Math.random(),
                                        title: 'New Job Booking',
                                        data: $('#newJobBookingForm').serializeArray(), 
                                        opacity: 0.75,
                                        height:210,
                                        width:670,
                                        overlayClose: false,
                                        escKey: false, 
                                        onComplete: function(){


                                            $(this).colorbox.resize({
                                            height: ($('#JobNetworkClientForm').height()+150)+"px"                                
                                        });



                                        }

                                        }); 
              });
                


});
  
    
</script>    


<form id="p_wildCardForm" name="p_wildCardForm">

    <fieldset>
         <legend >Wild Card Search</legend>
         
         <p>
             <br>You can create a wild card search by inserting * before or after the string you are searching for.
         </p>
         <p>
             Example1: Berry* would deliver all occurrences of Berry, Berryman, etc.
         </p>
          <p>
              Example2: *erry* would deliver all occurrences of Berry, Berryman, Merry, Merryman etc.
         </p>
         
    </fieldset>
    
</form>      

<form id="productSearchForm" name="productSearchForm" method="get"
            action="{$_subdomain}/index/productSearch" >
        
        <fieldset style="padding-right: 40px;padding-top: 18px;padding-bottom:18px">
            
            {* <legend title="{$ref}">Product Search</legend> *}
            <legend title="AP7101">Catalogue Search</legend>

            <p>
                <input type="text" name="productSearch" id="productSearch" 
                       value="{$productSearch}" class="text bigTextBox"  
                       title="Please enter the Stock Code, Catalogue Number or Description"/>
                <span style="float:right;">
                    <a  href="javascript:if ( validateProductForm() ) document.productSearchForm.submit();" >Search</a>
                </span>
            </p>
            
            <p style="margin-bottom: 0;" >
                
                {*
                {html_radios name='productGroup' options=$Product->search_fields selected=$Product->default_search_field separator=''}
                *}
                
		{if isset($superadmin)}
		    {if $superadmin}
			<a style="float:left;" tabIndex="16" href="#" id="p_wildCard"   >Wild Card Search</a>
			<img src="{$_subdomain}/css/Skins/{$_theme}/images/help.png" alt="Click here to see the help." title="Click here to see the help." width="20" height="20" id="p_wildCardHelp" style="float:left;padding-left:5px;padding-top:4px;" >
			<span class="noResults" >
			     {if $productNoResultsFlag}
			       No match found.   Re-enter the catalogue number, or select advanced catalogue search or<br> commence booking a new job.
			     {/if}
			</span>
		    {/if}
		{/if}
		
                <span style="float:right;margin:0;padding:0;" >
                    {if $session_user_id && $productNoResultsFlag eq false}
			{if isset($loggedin_user->Permissions["AP7038"]) || $loggedin_user->UserType == "Admin"}
			    <a href="#"  id="FreeTextJobBooking">Free Text Job Booking</a>
			{/if}
			{if $advancedCatalogue}&nbsp;&nbsp;&nbsp;&nbsp;{/if}
                    {/if}
                    
		    {if isset($advancedCatalogue)}
			{if $advancedCatalogue}
			    <a  href="{$_subdomain}/index/productAdvancedSearch">Advanced Catalogue Search</a>
			{/if}
		    {/if}
		    
                </span>
            </p>
        
        </fieldset>
        
 </form>

  
            
  
