<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
	<title>SkyLine - Questionaire</title>
	
	<!-- bin/jquery.slider.min.css -->
	<link rel="stylesheet" href="{$_subdomain}/questionnaires/css/jslider.css" type="text/css">
	<link rel="stylesheet" href="{$_subdomain}/questionnaires/css/jslider.blue.css" type="text/css">
	<link rel="stylesheet" href="{$_subdomain}/questionnaires/css/jslider.plastic.css" type="text/css">
	<link rel="stylesheet" href="{$_subdomain}/questionnaires/css/jslider.round.css" type="text/css">
	<link rel="stylesheet" href="{$_subdomain}/questionnaires/css/jslider.round.plastic.css" type="text/css">
  <!-- end -->

	<script type="text/javascript" src="{$_subdomain}/questionnaires/js/jquery-1.7.1.js"></script>
	
	<!-- bin/jquery.slider.min.js -->
	<script type="text/javascript" src="{$_subdomain}/questionnaires/js/jshashtable-2.1_src.js"></script>
	<script type="text/javascript" src="{$_subdomain}/questionnaires/js/jquery.numberformatter-1.2.3.js"></script>
	<script type="text/javascript" src="{$_subdomain}/questionnaires/js/tmpl.js"></script>
	<script type="text/javascript" src="{$_subdomain}/questionnaires/js/jquery.dependClass-0.1.js"></script>
	<script type="text/javascript" src="{$_subdomain}/questionnaires/js/draggable-0.1.js"></script>
	<script type="text/javascript" src="{$_subdomain}/questionnaires/js/jquery.slider.js"></script>
        {* written by srinivas for enableing textarea resizeable in IE *}
        {if $browser=='ie' }               	
		<script type="text/javascript" src="{$_subdomain}/questionnaires/js/jquery.textareaAutoResize.js"></script>
		
		<style type="text/css">
			div.grippie {
				background:#EEEEEE url({$_subdomain}/questionnaires/img/grippie.png) no-repeat scroll center 2px;
				border-color:#DDDDDD;
				border-style:solid;
				border-width:0pt 1px 1px;
				cursor:s-resize;
				height:9px;
				overflow:hidden;
			}
			.resizable-textarea textarea {
				display:block;
				margin-bottom:0pt;
				width:95%;
				height: 20%;
			}
		</style>
        {/if }        
  <!-- end -->

        <link rel="stylesheet" href="{$_subdomain}/questionnaires/css/style.css" type="text/css"  media="screen"/>
        
        {* Skin Stylesheet  *}
        {if $_skin ne ''}
        <link rel="stylesheet" href="{$_subdomain}/questionnaires{$_skin}/css/style.css" type="text/css"  />
        {/if}      
        <script>
        var key_questions = new Array(
                'definitely not',
                'definitely not',
                'be highly unlikely to',
                'be highly unlikely to',
                'be unlikely to',
                'be unlikely to',
                'be unlikely to',
                'possibly',
                'probably',
                'definitely',
                'most definitely'
            );
        var additional_questions = new Array(
                'extremely poor',
                'really poor',
                'very poor',
                'poor',
                'slightly less than satisfactory',
                'satisfactory',
                'slightly more than satisfactory',
                'acceptable',
                'good',
                'very good',
                'excellent'
            );
               
        $(document).ready(function() {
        {if $browser=='ie' }    
        $('textarea.resizable:not(.processed)').TextAreaResizer();
         {/if }    
    $('#submitButton').click(function(e) {
                        $("#questionnaire_form").submit();
                        e.preventDefault();
                    });
            $("#SliderSingle7").slider( { 
                      from: 0, to: 10, step: 1, round: 0, format: { format: '##0'}, dimension: '&nbsp;', skin: "round",
                      onstatechange: function( value ){
                                       $('#key-question-rating').html(key_questions[value]);
                                       $('#key-question-subject').html('{$brand}');
                                     }
                  } );
            $("#SliderSingle8").slider( { 
                      from: 0, to: 10, step: 1, round: 0, format: { format: '##0'}, dimension: '&nbsp;', skin: "round",
                      onstatechange: function( value ){
                                       $('#key-question-rating').html(key_questions[value]);
                                       $('#key-question-subject').html('{$manufacturer}');
                                     } 
                  } );
            $("#SliderSingle9").slider( { 
                      from: 0, to: 10, step: 1, round: 0, format: { format: '##0'}, dimension: '&nbsp;', skin: "round",
                      onstatechange: function( value ){
                                       $('#key-question-rating').html(key_questions[value]);
                                       $('#key-question-subject').html('{$service_agent}');
                                     } 
                  } );
            $("#SliderSingle2").slider({ 
                      from: 0, to: 10, step: 1, round: 0, format: { format: '##0', locale: 'de' }, dimension: '&nbsp;', skin: "round",
                      onstatechange: function( value ){
                                       $('#additional-question-rating').html(additional_questions[value]);
                                     } 
                  });
            $("#SliderSingle3").slider({ 
                      from: 0, to: 10, step: 1, round: 0, format: { format: '##0', locale: 'de' }, dimension: '&nbsp;', skin: "round",
                      onstatechange: function( value ){
                                       $('#additional-question-rating').html(additional_questions[value]);
                                     } 
                  });
            $("#SliderSingle4").slider({ 
                      from: 0, to: 10, step: 1, round: 0, format: { format: '##0', locale: 'de' }, dimension: '&nbsp;', skin: "round",
                      onstatechange: function( value ){
                                       $('#additional-question-rating').html(additional_questions[value]);
                                     } 
                  });
            $("#SliderSingle5").slider({ 
                      from: 0, to: 10, step: 1, round: 0, format: { format: '##0', locale: 'de' }, dimension: '&nbsp;', skin: "round",
                      onstatechange: function( value ){
                                       $('#additional-question-rating').html(additional_questions[value]);
                                     } 
                  });
            $("#SliderSingle6").slider({ 
                      from: 0, to: 10, step: 1, round: 0, format: { format: '##0'}, dimension: '&nbsp;', skin: "round",
                      onstatechange: function( value ){
                                       $('#additional-question-rating').html(additional_questions[value]);
                                     } 
                  });
        });
        </script>

</head>
<body>
  
    <form id="questionnaire_form" name="questionnaire_form" action="{$_subdomain}/questionnaire" method="post">
        
        {**********************************************************************
          Note: Questionnaire System fields begin with an underscore character.
                These fields do not get copied to the questionnare data table.
                _questionaire_type = the smarty template filename (5 chars)
                _job_id = the skyline job number
                _customer_id = customers primary key
                _log = the customer email guid (primary key to questionnaire_log table)
                
           ALL of these hidden fields MUST be present on every Questionnaire form
         **********************************************************************}
        <input type="hidden" name="_questionnaire_type" value="A0001" />
        <input type="hidden" name="_job_id" value="{$job_id}" />
        <input type="hidden" name="_customer_id" value="{$customer_id}" />
        <input type="hidden" name="_brand_id" value="{$brandID}" />
        <input type="hidden" name="_log" value="{$log}" />
        
        <div class="main" style="margin-top: -78px;">
            <div class="logo">

            </div>
            <div class="content">
                <img style="position:relative;float:left" name="" src="{$_subdomain}/images/brandLogos/{$logo}" alt="">
                <img src="{$_subdomain}/questionnaires{$_skin}/img/questionaire.png" style="padding-left:{410-$logowidth}px" />
                <div class="line">
                    <div>
                        <div class="text">CUSTOMER SATISFACTION QUESTIONNAIRE</div>
                    </div>
                </div>
                <div class="motto">We would like to thank you in advance for taking the time to complete this questionnaire.<br />
Your feedback is important and will help us to maintain a quality service.
 </div>
                <!--div class="line2"></div-->
            </div>

        </div> 
        <div style="background:#ffffff; padding:10px 0px; border-top:1px solid #CCC;border-bottom:1px solid #eaeaea;">
             <table width="960" border="0" cellspacing="0" cellpadding="10" style="margin:30px auto 0px auto; font-size:12px;">
                <tr>
                    <td><span class="niceLetters" style="color:#666;">Who suggested our services to you?</span></td>
                </tr>  
                {if isset($datarow.Who_suggested_our_services_to_you)}{assign var=radio value="|"|explode:$datarow.Who_suggested_our_services_to_you} {/if}
                <tr>
                    <td>
                        {assign var=last value=$radio|@end}                         
                        {foreach $radio as $r}                         
                            <label {if $last==$r} style="margin-right:0px" {else} style="margin-right: 25px" {/if}  > 
                                <input type="radio" value="{$r|escape:'html'}" name="Who_suggested_our_services_to_you[]"> {$r|escape:'html'} 
                            </label>                                 
                        {/foreach}
                      <!--  <label style="margin-right: 40px"> <input type="radio" value="retailer" name="Who_suggested_our_services_to_you[]"> {$radio[0]} </label>                        
                       <label style="margin-right: 30px"><input type="radio" value="insurer_extended_warrantor" name="Who_suggested_our_services_to_you[]"> {$radio[1]}</label>                        
                        <label style="margin-right: 30px"><input type="radio" value="advertisement" name="Who_suggested_our_services_to_you[]"> {$radio[2]}</label>
                        
                        <label style="margin-right: 40px"><input type="radio" value="manufacturer" name="Who_suggested_our_services_to_you[]"> {$radio[3]}</label>
                        <label  style="margin-right: 30px"><input type="radio" value="web_search" name="Who_suggested_our_services_to_you[]"> {$radio[4]}</label>
                        <label><input type="radio" value="personal _recommendation" name="Who_suggested_our_services_to_you[]"> {$radio[5]}</label>
                      -->
                </tr>
            </table>
            
            
            <!--div class="content">
            <div class="line2"></div>
            </div-->

            <table width="960" border="0" cellspacing="0" cellpadding="10" style="margin:30px auto 0px auto;">
                <tr>
                    <td width="475px"><span class="niceLetters">Key Questions</span></td>
                    <td></td>
                </tr>
            </table>
            <table width="960" border="0" cellspacing="0" cellpadding="10" style="margin:0px auto 30px auto; border:1px solid #BBBBBB;">
                <tr>
                    <td width="49%" style="border-bottom:4px solid #D2D2D2;" align="right"> 
                        As a result of your service experience how likely are you to recommend the following to a friend or relative?
                    </td>
                    <td style="border-bottom:4px solid #D2D2D2;">
                        <span class="niceLetters" style="color:#666; font-size:14px; text-align:left;">
                            I would <span id="key-question-rating"></span> recommend <span id="key-question-subject"></span> 
                        </span>
                    </td>
                </tr>
                <tr class="odd">
                    <td width="49%" style="border-bottom:1px solid #D2D2D2;" align="right">{$brand} : who retailed your product</td><td style="border-bottom:1px solid #D2D2D2;">
                        <!-- slider seven  -->
                        <div class="layout-slider-settings">

                        </div>
                        <div class="layout-slider">
                            <input id="SliderSingle7" type="slider" name="key_question_brand" value="20" />
                        </div>
                        <!-- end of slider seven -->
                    </td>
                    {if $manufacturer ne $brand}
                    <tr class="even">
                        <td width="49%" style="border-bottom:1px solid #D2D2D2;" align="right">{$manufacturer} : who manufactured your product</td><td style="border-bottom:1px solid #D2D2D2;">
                            <!-- slider eight  -->
                            <div class="layout-slider-settings">

                            </div>
                            <div class="layout-slider">
                                <input id="SliderSingle8" type="slider" name="key_question_manufacturer" value="20" />
                            </div>
                            <!-- end of slider eight -->
                        </td>
                    </tr>
                    <tr class="odd">
                    {else}
                    <tr class="even">   
                    {/if}
                    <td width="49%" align="right" nowrap> {$service_agent} : who serviced your product </td><td>
                        <!-- slider nine  -->
                        <div class="layout-slider-settings">

                        </div>
                        <div class="layout-slider">
                            <input id="SliderSingle9" type="slider" name="key_question_service_agent" value="20" />
                        </div>
                        <!-- end of slider nine -->
                    </td>
            </table>

            <table width="960" border="0" cellspacing="0" cellpadding="10" style="margin:30px auto 0px auto;">
                <table width="960" border="0" cellspacing="0" cellpadding="10" style="margin:30px auto 0px auto;">
                    <tr>
                        <td width="475px"><span class="niceLetters">Additional Questions</span></td>
                        <td></td>
                    </tr>
                </table>  
            </table>
            <table width="960" border="0" cellspacing="0" cellpadding="10" style="margin:0px auto 0px auto; border:1px solid #BBBBBB">
                <tr class="odd">
                    <td width="49%" style="border-bottom:4px solid #D2D2D2;" align="right">Please rate the service experience you received</td>
                    <td style="border-bottom:4px solid #D2D2D2;">
                        <span style="color:#666;" class="niceLetters">
                            I rate the service I received as <span id="additional-question-rating"></span> 
                        </span>
                    </td>
                </tr>
                <tr class="even">
                    <td style="border-bottom:1px solid #D2D2D2;" align="right" >{$datarow.ease_of_booking}</td>
                    <td style="border-bottom:1px solid #D2D2D2;">     <!-- slider two  -->
                        <div class="layout-slider-settings">

                        </div>
                        <div class="layout-slider">
                            <input id="SliderSingle2" type="slider" name="ease_of_booking" value="20" />
                        </div>
                        <!-- end of slider two  --></td>
                </tr>
                <tr class="odd">
                    <td style="border-bottom:1px solid #D2D2D2;" align="right" > {$datarow.speed_of_service_first_visit}</td>
                    <td style="border-bottom:1px solid #D2D2D2;"><!-- slider three  -->
                        <div class="layout-slider-settings">

                        </div>
                        <div class="layout-slider">
                            <input id="SliderSingle3" type="slider" name="speed_of_service_first_visit" value="20" />
                        </div>
                        <!-- end of slider three  --></td>
                </tr>
                <tr class="even">
                    <td style="border-bottom:1px solid #D2D2D2;" align="right" > {$datarow.speed_of_service_overall} </td>
                    <td style="border-bottom:1px solid #D2D2D2;"><!-- slider four  -->
                        <div class="layout-slider-settings">

                        </div>
                        <div class="layout-slider">
                            <input id="SliderSingle4" type="slider" name="speed_of_service_overall" value="20" />
                        </div>
                        <!-- end of slider four  --></td>
                </tr>
                <tr class="odd">
                    <td style="border-bottom:1px solid #D2D2D2;" align="right" >{$datarow.communication}</td>
                    <td style="border-bottom:1px solid #D2D2D2;"><!-- slider five  -->
                        <div class="layout-slider-settings">

                        </div>
                        <div class="layout-slider">
                            <input id="SliderSingle5" type="slider" name="communication" value="20" />
                        </div>
                        <!-- end of slider five --></td>
                </tr>
                <tr class="even">
                    <td align="right" > {$datarow.engineer} </td>
                    <td><!-- slider six  -->
                        <div class="layout-slider-settings">

                        </div>
                        <div class="layout-slider">
                            <input id="SliderSingle6" type="slider" name="engineer" value="20" />
                        </div>
                        <!-- end of slider six --></td>
                </tr>
            </table>

        </div> <!-- end of gray background -->

        <div style="background:#FFF; padding:20px 0px;margin-top:0px;">
            <table width="960" cellspacing="0" cellpadding="0" border="0" style="margin:0px auto 0px auto;">
                <tbody>
                    <tr>
                        <td><span class="niceLetters" style="color:#666;margin-left:0px;">Additional Comments/Testimonial</span></td>
                        </tr>
                        
                    <tr>
                        <td style="padding-top:10px">Please leave any further comments and feedback in the box provided.</td>
                    </tr>
                    <tr>
                        <td><textarea  class="resizable" name="comments" style="width:950px; height:200px" ></textarea></td>
                    </tr>
                    <tr>
                        <td style="padding-top:10px">Please indicate below if you are happy for your comments to be used as a testimonial of our services. Your identity will not be revealed.</td>
                    </tr>
                    <tr>
                        <td>
                            Yes I am happy for my comments to be used  <input type="checkbox" value="yes" name="permission_to_use_comments[]">              
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="960" border="0" cellspacing="0" cellpadding="10" style="margin:10px auto;">
                <tr><td><a id="submitButton" href="#" style="text-align: center;float:right; width: 140px;background:#3576BC; color:#fff; padding:10px 15px; text-decoration:none; font-size:14px;" />SUBMIT COMPLETED QUESTIONNAIRE</a></td></tr>
            </table>  
        </div>

    </form>

{* ***********************************************************************
    
    IMPORTANT NOTE: The closing body and html tags are now appended to the output stream
                    in index.php.
         
    </body>
                
</html>

**************************************************************************** *}    