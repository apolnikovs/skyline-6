{extends "DemoLayout.tpl"}


    {block name=config}
    {$Title = $page['Text']['page_title']|escape:'html'}
    {$PageId = $PricingStructure}
    {/block}
    {block name=afterJqueryUI}
        <script type="text/javascript" src="{$_subdomain}/js/jquery.combobox.js"></script>
        <link rel="stylesheet" href="{$_subdomain}/css/themes/pccs/style.css" type="text/css" />
        <style type="text/css" >
            .ui-combobox-input {
                 width:300px;
             }  
        </style>
    {/block}
    {block name=scripts}



    {*<link rel="stylesheet" href="{$_subdomain}/css/colorbox/colorbox.css" type="text/css" charset="utf-8" />*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.jlabel-1.3.min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.form.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.validate.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/additional-methods.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTables.min.js"></script>*}
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.colorbox-min.js"></script>*} 
    {*<script type="text/javascript" src="{$_subdomain}/js/jquery.dataTablesPCCS.js"></script>*}
    


    <script type="text/javascript">
    
     var $statuses = [
                    {foreach from=$statuses item=st}
                       ["{$st.Name}", "{$st.Code}"],
                    {/foreach}
                    ]; 
     
     var $jobSites = [
                    {foreach from=$jobSites item=js}
                       ["{$js.JobSiteName}", "{$js.JobSiteCode}"],
                    {/foreach}
                    ]; 
               
    
                    
        
    function gotoEditPage($sRow)
    {
        
         
        $('#updateButtonId').removeAttr('disabled').removeClass('gplus-blue-disabled').addClass('gplus-blue');
        $('#updateButtonId').trigger('click');
       
       
    }

    /**
    *  This is used to change color of the row if its in-active.
    */
    function inactiveRow(nRow, aData)
    {
          
        if (aData[8]==$statuses[1][1])
        {  
            $(nRow).addClass("inactive");

            $('td:eq(7)', nRow).html( $statuses[1][0] );
        }
        else
        {
            $(nRow).addClass("");
            $('td:eq(7)', nRow).html( $statuses[0][0] );
        }
        
        
        if (aData[4]==$jobSites[1][1])
        {  
          
            $('td:eq(3)', nRow).html( $jobSites[1][0] );
        }
        else
        {
            $('td:eq(3)', nRow).html( $jobSites[0][0] );
        }
        
    }
    
    
    
   
 

    $(document).ready(function() {
        $("#nId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/pricingStructure/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/');
            }
        });
        $("#cId").combobox({
            change: function() {
                $(location).attr('href', '{$_subdomain}/ProductSetup/pricingStructure/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/');
            }
        });



                   //Click handler for finish button.
                    $(document).on('click', '#finish_btn', 
                                function() {
                                
                                     $(location).attr('href', '{$_subdomain}/SystemAdmin/index/productSetup');

                                });


                      /* Add a change handler to the network dropdown - strats here*/
                        /*$(document).on('change', '#nId', 
                            function() {
                                                           
                                $(location).attr('href', '{$_subdomain}/ProductSetup/pricingStructure/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'); 
                            }      
                        );*/
                      /* Add a change handler to the network dropdown - ends here*/
                      
                      
                      
                      
                     /* Add a change handler to the client dropdown - strats here*/
                        /*$(document).on('change', '#cId', 
                            function() {
                                
                                $(location).attr('href', '{$_subdomain}/ProductSetup/pricingStructure/'+urlencode($("#nId").val())+"/"+urlencode($("#cId").val())+'/'); 
                            }      
                        );*/
                      /* Add a change handler to the client dropdown - ends here*/ 
                      
                      
                      
                      
                      //change handler for network dropdown box on update/insert popup page.
                      /*$(document).on('change', '#NetworkID', 
                                function() {
                                    
                                   $CompletionStatusDropDownList = $ManufacturerDropDownList =  $clientDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                                    
                                
                                    var $NetworkID = $("#NetworkID").val();
                                
                                    if($NetworkID && $NetworkID!='')
                                        {
                                           
                                            //Getting clients of network
                                            $.post("{$_subdomain}/ProductSetup/pricingStructure/getClients/"+urlencode($NetworkID)+"/",        

                                            '',      
                                            function(data){
                                                    var $networkClients = eval("(" + data + ")");

                                                    if($networkClients)
                                                    {
                                                        for(var $i=0;$i<$networkClients.length;$i++)
                                                        {

                                                            $clientDropDownList += '<option value="'+$networkClients[$i]['ClientID']+'" >'+$networkClients[$i]['ClientName']+'</option>';
                                                        }
                                                    }

                                                    $("#ClientID").html($clientDropDownList);

                                            });
                                  
                                            
                                             //Getting manufacturers of network
                                            $.post("{$_subdomain}/ProductSetup/pricingStructure/getManufacturers/"+urlencode($NetworkID)+"/",        

                                            '',      
                                            function(data){
                                                    var $networkManufacturers = eval("(" + data + ")");

                                                    if($networkManufacturers)
                                                    {
                                                        for(var $i=0;$i<$networkManufacturers.length;$i++)
                                                        {

                                                            $ManufacturerDropDownList += '<option value="'+$networkManufacturers[$i]['ManufacturerID']+'" >'+$networkManufacturers[$i]['ManufacturerName']+'</option>';
                                                        }
                                                    }

                                                    $("#ManufacturerID").html($ManufacturerDropDownList);

                                            });
                                                
                                                
                                                
                                            //Getting completion statuses of network    
                                             $.post("{$_subdomain}/ProductSetup/pricingStructure/getCompletionStatuses/"+urlencode($NetworkID)+"/",        

                                            '',      
                                            function(data){
                                                    var $networkCompletionStatuses = eval("(" + data + ")");

                                                    if($networkCompletionStatuses)
                                                    {
                                                        for(var $i=0;$i<$networkCompletionStatuses.length;$i++)
                                                        {

                                                            $CompletionStatusDropDownList += '<option value="'+$networkCompletionStatuses[$i]['CompletionStatusID']+'" >'+$networkCompletionStatuses[$i]['CompletionStatusName']+'</option>';
                                                        }
                                                    }

                                                    $("#CompletionStatusID").html($CompletionStatusDropDownList);

                                            });    
                                                
                                                
                                                
                                        }
                                    

                                });*/ 
                      
                      
                      
                      
                    //change handler for client dropdown box on update/insert popup page.
                      /*$(document).on('change', '#ClientID', 
                                function() {
                                    
                                    $utDropDownList = '<option value="">{$page['Text']['select_default_option']|escape:'html'}</option>';
                                
                                    var $ClientID = $("#ClientID").val();
                                
                                    if($ClientID && $ClientID!='')
                                        {
                                           
                                            
                                            $.post("{$_subdomain}/ProductSetup/pricingStructure/getUnitTypes/"+urlencode($ClientID)+"/",        

                                            '',      
                                            function(data){
                                                    var $clientUnitTypes = eval("(" + data + ")");

                                                    if($clientUnitTypes)
                                                    {
                                                        for(var $i=0;$i<$clientUnitTypes.length;$i++)
                                                        {

                                                            $utDropDownList += '<option value="'+$clientUnitTypes[$i]['UnitTypeID']+'" >'+$clientUnitTypes[$i]['UnitTypeName']+'</option>';
                                                        }
                                                    }

                                                    $("#UnitTypeID").html($utDropDownList);

                                            });
                                  
                                                
                                        }
                                    

                                });*/   
                      
                      
                      
                      

                    /* =======================================================
                    *
                    * set tab on return for input elements with form submit on auto-submit class...
                    *
                    * ======================================================= */

                    $('input[type=text],input[type=password]').keypress( function( e ) {
                            if (e.which == 13) {
                                $(this).blur();
                                if ($(this).hasClass('auto-submit')) {
                                    $('.auto-hint').each(function() {
                                        $this = $(this);
                                        if ($this.val() == $this.attr('title')) {
                                            $this.val('').removeClass('auto-hint').addClass('auto-hint-hide');
                                            if ($this.hasClass('auto-pwd')) {
                                                $this.prop('type','password');
                                            }
                                        }
                                    } );
                                    $(this).get(0).form.onsubmit();
                                } else {
                                    $next = $(this).attr('tabIndex') + 1;
                                    $('[tabIndex="'+$next+'"]').focus();
                                }
                                return false;
                            }
                        } );        


                        
                    
                    
                    $('#PricingStructureResults').PCCSDataTable( {
                              "aoColumns": [ 
                                                        /* UnitPricingStructureID */  {  "bVisible":    false },    
                                                        /* ClientID */   null,
                                                        /* UnitTypeID */   null,
                                                        /* ManufacturerID */   null,
                                                        /* JobSite */   null,
                                                        /* CompletionStatusID */   null,
                                                        /* ServiceRate */   null,
                                                        /* ReferralFee */   null,
                                                        /* Status */  null
                                                ],
                               
                            displayButtons:  "UA",
                            addButtonId:     'addButtonId',
                            addButtonText:   '{$page['Buttons']['insert']|escape:'html'}',
                            createFormTitle: '{$page['Text']['insert_page_legend']|escape:'html'}',
                            createAppUrl:    '{$_subdomain}/ProductSetup/pricingStructure/insert/'+urlencode("{$nId}")+"/"+urlencode("{$cId}")+'/',
                            createDataUrl:   '{$_subdomain}/ProductSetup/ProcessData/PricingStructure/',
                            createFormFocusElementId:'NetworkID',
                            formInsertButton:'insert_save_btn',
                           
                            frmErrorRules:   {
                                                    NetworkID:
                                                        {
                                                            required: true
                                                        },
                                                    ClientID:
                                                        {
                                                            required: true
                                                        },
                                                    UnitTypeID:
                                                        {
                                                            required: true
                                                        },
                                                    ManufacturerID:
                                                        {
                                                            required: true
                                                        },
                                                    JobSite:
                                                        {
                                                            required: true
                                                        },
                                                    CompletionStatusID:
                                                        {
                                                            required: true
                                                        },
                                                    ServiceRate:
                                                        {
                                                            number: true
                                                        },
                                                    ReferralFee:
                                                        {
                                                            required: true,
                                                            number: true
                                                        }    
                                                        
                                                        
                                                        
                                             },
                                                
                           frmErrorMessages: {
                           
                                                    NetworkID:
                                                        {
                                                           required: "{$page['Errors']['network']|escape:'html'}"
                                                        },
                                                    ClientID:
                                                        {
                                                            required: "{$page['Errors']['client']|escape:'html'}"
                                                        },
                                                    UnitTypeID:
                                                        {
                                                            required: "{$page['Errors']['unit_type']|escape:'html'}"
                                                        },
                                                    ManufacturerID:
                                                        {
                                                            required: "{$page['Errors']['manufacturer']|escape:'html'}"
                                                        },
                                                    JobSite:
                                                        {
                                                            required: "{$page['Errors']['job_site']|escape:'html'}"
                                                        },
                                                    CompletionStatusID:
                                                        {
                                                            required: "{$page['Errors']['completion_status']|escape:'html'}"
                                                        },
                                                    ServiceRate:
                                                        {
                                                            number: "{$page['Errors']['price_format']|escape:'html'}"
                                                        },
                                                    ReferralFee:
                                                        {
                                                            required: "{$page['Errors']['referral_fee']|escape:'html'}",
                                                            number: "{$page['Errors']['price_format']|escape:'html'}"
                                                        }  
                           
                                                     
                                              },                     
                            
                            popUpFormWidth:  750,
                            popUpFormHeight: 430,
                            
                            
                            updateButtonId:  'updateButtonId',
                            updateButtonText:'{$page['Buttons']['edit']|escape:'html'}',
                            updateFormTitle: '{$page['Text']['update_page_legend']|escape:'html'}',
                            updateAppUrl:    '{$_subdomain}/ProductSetup/pricingStructure/update/',
                            updateDataUrl:   '{$_subdomain}/ProductSetup/ProcessData/PricingStructure/',
                            formUpdateButton:'update_save_btn',
                            updateFormFocusElementId:'NetworkID',
                            
                            colorboxFormId:  "PricingStructureForm",
                            frmErrorMsgClass:"fieldError",
                            frmErrorElement: "label",
                            htmlTablePageId: 'PricingStructureResultsPanel',
                            htmlTableId:     'PricingStructureResults',
                            fetchDataUrl:    '{$_subdomain}/ProductSetup/ProcessData/PricingStructure/fetch/'+urlencode("{$nId}")+"/"+urlencode("{$cId}"),
                            formCancelButton:'cancel_btn',
                            
                            fnRowCallback:          'inactiveRow',
                            searchCloseImage:'{$_subdomain}/css/Skins/{$_theme}/images/close.png',
                            
                            dblclickCallbackMethod: "gotoEditPage",
                            tooltipTitle:   "{$page['Text']['tooltip_title']|escape:'html'}",
                            
                            iDisplayLength:  25,
                            formDataErrorMsgId: "suggestText",
                            frmErrorSugMsgClass:"formCommonError",
                            sDom: 'ft<"#dataTables_command">rpli',
                            bottomButtonsDivId:'dataTables_command'


                        });
                      

                        
                   

    });

    </script>

    {/block}

    {block name=body}

    <div class="breadcrumb">
        <div>

            <a href="{$_subdomain}/SystemAdmin" >{$page['Text']['system_admin_home_page']|escape:'html'}</a> / <a href="{$_subdomain}/SystemAdmin/index/productSetup" >{$page['Text']['product_setup']|escape:'html'}</a> / {$page['Text']['page_title']|escape:'html'}

        </div>
    </div>



    <div class="main" id="home" >

               <div class="ServiceAdminTopPanel" >
                    <form id="PricingStructureTopForm" name="PricingStructureTopForm" method="post"  action="#" class="inline">

                        <fieldset>
                        <legend title="" >{$page['Text']['legend']|escape:'html'}</legend>
                        <p>
                            <label>{$page['Text']['description']|escape:'html'}</label>
                        </p> 

                        </fieldset> 


                    </form>
                </div>  

                        
                <div class="ServiceAdminResultsPanel" id="PricingStructureResultsPanel" >
                   <form name="listsForm" id="serviceAdminListForm">  
                    
                    {if $SupderAdmin eq true} 
                        {$page['Labels']['service_network_label']|escape:'html'}
                        <select name="nId" id="nId"  >
                            <option value="" {if $nId eq ''}selected="selected"{/if}>{$page['Text']['select_service_network']|escape:'html'}</option>

                            {foreach $networks as $network}

                                <option value="{$network.NetworkID}" {if $nId eq $network.NetworkID}selected="selected"{/if}>{$network.CompanyName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        <br /><span style="padding-right: 59px;">{$page['Labels']['client_label']|escape:'html'}</span>
                        <select name="cId" id="cId"  >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                    {else if $NetworkUser eq true}
                        
                        
                        <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" >   
                        {$page['Labels']['client_label']|escape:'html'}
                        <select name="cId" id="cId"  >
                            <option value="" {if $cId eq ''}selected="selected"{/if}>{$page['Text']['select_client']|escape:'html'}</option>

                            {foreach $clients as $client}

                                <option value="{$client.ClientID}" {if $cId eq $client.ClientID}selected="selected"{/if}>{$client.ClientName|escape:'html'}</option>

                            {/foreach}
                        </select>
                        
                        
                    {else if $ClientUser eq true}
                        
                        <input type="hidden" name="nId" id="nId" value="{$nId|escape:'html'}" > 
                        <input type="hidden" name="cId" id="cId" value="{$cId|escape:'html'}" >  
                       
                    {/if} 
                    
                     </form>
                    
                    
                    
                    <form id="PricingStructureResultsForm" class="dataTableCorrections">
                        <table id="PricingStructureResults" border="0" cellpadding="0" cellspacing="0" class="browse" >
                            <thead>
                                    <tr>
                                            <th></th> 
                                            <th title="{$page['Text']['client']|escape:'html'}" >{$page['Text']['client']|escape:'html'}</th>
                                            <th title="{$page['Text']['unit_type']|escape:'html'}" >{$page['Text']['unit_type']|escape:'html'}</th>
                                            <th title="{$page['Text']['manufacturer']|escape:'html'}" >{$page['Text']['manufacturer']|escape:'html'}</th>
                                            <th title="{$page['Text']['job_site']|escape:'html'}" >{$page['Text']['job_site']|escape:'html'}</th>
                                            <th title="{$page['Text']['completion_status']|escape:'html'}" >{$page['Text']['completion_status']|escape:'html'}</th>
                                            <th title="{$page['Text']['service_rate']|escape:'html'}" >{$page['Text']['service_rate']|escape:'html'}</th>
                                            <th title="{$page['Text']['referral_fee']|escape:'html'}" >{$page['Text']['referral_fee']|escape:'html'}</th>
                                            <th title="{$page['Text']['status']|escape:'html'}"  >{$page['Text']['status']|escape:'html'}</th>
                                            
                                    </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>  
                     </form>
                </div>        

                <div class="bottomButtonsPanel" >
                    <hr>
                    
                    <button id="finish_btn" type="button" class="gplus-blue rightBtn" ><span class="label">{$page['Buttons']['finish']|escape:'html'}</span></button>
                    
                </div>        
               
    </div>
                        
                        



{/block}



