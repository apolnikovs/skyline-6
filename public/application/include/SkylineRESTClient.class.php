<?php

/**
 * SkylineRESTClient.class.php
 * 
 * Implementatrion of a REST Client for the Skyline API
 *
 * @author     Andrew Williams <a.williams@pccsuk.com>
 * @copyright  2012 - 2013 PC Control Systems
 * @link       
 * @version    1.5
 * 
 * Changes
 * Date        Version Author                Reason
 * 23/01/2012  1.0     Andrew Williams       Initial Version 
 * 07/09/2012  1.1     Brian Etherington     added callRMATracker 
 * 19/09/2012  1.2     Andrew Williams       Added callServiceBase
 * 28/09/2012  1.3     Andrew Williams       Added callViamente 
 * 16/01/2013  1.4     Andrew Williams       Added sbPutDiaryWizardSelection
 * 19/04/2013  1.5     Andrew Williams       Data Integrity Check - Referesh Process - Added sbSkylineRefresh
 ******************************************************************************/

require_once('CustomRESTClient.class.php');
require_once('xml2array.php');

class SkylineRESTClient extends CustomRESTClient {
    
    private $web_service = "RMASkylineService";
               
    public function __construct($controller) { 
        
        parent::__construct($controller);
        
        if (isset($this->controller->config['RMATracker']['WebService'])) {
            $this->web_service = $this->controller->config['RMATracker']['WebService'];
        }
        
    }
    
    /****************
    *               *
    *  RMA Tracker  *
    *               *
    ****************/
   
    
    private function callRMATracker($path, $params, $method='POST') {
        
         $url = $params['Site'];                  /* Info for API to be called */
        
        $this->setUsername($params['username']);  /* Set the username and password for http authentication */
        $this->setPassword($params['password']);
        
        if ($url[strlen($url) - 1] !== '/') {
            $url .= '/';           
        }
        
        unset($params['username']);               /* Don't want username and password passed in params */
        unset($params['password']);
        
        unset($params['Site']);                   /* Don't want to pass site address as parameter */
        
        return($this->execute($url.$this->web_service.$path, $params, $method));       
    }
     
    
    public function PutNewJob($params) {
        return $this->callRMATracker('/PutNewJob', $params, 'POST');
    }
    
    public function PutJobDetails($params) {
        //$this->controller->log('/PutJobDetails: '.var_export($params,true));
        return $this->callRMATracker('/PutJobDetails', $params, 'POST');
    }
    
    
    /*****************
    *                *
    *  Service Base  *
    *                *
    *****************/
    
    private function callServiceBase($path, $params, $method='POST') {
        
        $url = $params['Site'];                                                 /* Site with the API to be called */
        
        $this->setUsername($params['username']);                                /* Set the username and password for http authentication */
        $this->setPassword($params['password']);
        
        if ($url[strlen($url) - 1] !== '/') {
            $url .= '/';           
        }
        
        unset($params['username']);                                             /* Don't want username and password passed in params */
        unset($params['password']);
        unset($params['Site']);                                                 /* Don't want to pass site address as parameter */
        
        return($this->execute($url.$path, $params, $method));       
    }
    
    public function sbPutSettings($params) {
        return $this->callServiceBase('PutSettings', $params, 'POST');
    }
    
    public function sbDiaryPutAppointmentDetails($params) {
        return $this->callServiceBase('PutAppointmentDetails', $params, 'POST');
    }
    
    public function sbDiaryDeleteAppointment($params) {
        return $this->callServiceBase('DeleteAppointment', $params, 'POST');
    }
    
    public function sbRoutedAppointments($params) {
        return $this->callServiceBase('RoutedAppointments', $params, 'POST');
    }
    
    public function sbPutDiaryWizardSelection($params) {
        return $this->callServiceBase('PutDiaryWizardSelection', $params, 'POST');
    }
    
    public function sbSkylineRefresh($params) {
        return $this->callServiceBase('SkylineRefresh', $params, 'GET');
    }
    
    /*************
    *            *
    *  Viamente  *
    *            *
    *************/
    
    private function callViamente($path, $params, $method='POST') {
        //$url =  'http://api.viamente.com';                                      /* Live */
        $url = 'http://apitest.viamente.com';                                   /* Test */
        /*$key = $params['Key'];*/
        
        /*unset($params['Key']);*/
        //unset($params['Site']);
        //$key = 'e432ad91-edf8-4019-a0e8-3764ef2ae15a';                          /* Temporary key */
        $key = 'SLPY-PCRU-VNJC-ZTMV'; 
        
        return($this->execute($url.$path.'&key='.$key, null, $method, $params, array('Content-type: application/json')));
    }

}

?>
